#!/bin/bash
# update fonts.dir and generate fonts.alias
if [ ! -f fonts.dir ]; then mkfontdir;fi
rm -f fonts.alias;touch fonts.alias
for X in `grep ksx1001.1998 fonts.dir |awk '{print $2;}'`; do \
echo "$X $X" |sed 's/ksx1001\.1998/ksc5601\.1987/' >>fonts.alias; done
